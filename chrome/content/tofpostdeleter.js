
var tofpostdeleter = function () {
	var prefManager = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);
	return {
		init : function () {},

		loadXMLDoc : function(url,callback) {
		    var xmlhttp;
		    if (window.XMLHttpRequest) {
		        // code for IE7+, Firefox, Chrome, Opera, Safari
		        xmlhttp = new XMLHttpRequest();
		    } else {
		        // code for IE6, IE5
		        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		    xmlhttp.onreadystatechange = function() {
		        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		        	callback(xmlhttp.responseText);
		        }
		    }
		    xmlhttp.open("GET", url, true);
		    xmlhttp.send();
		},

		writeToFile : function(msg) {
			try{
				//create proper path for file
				var theFile = tofpostdeleter.savePath+'deleted_tof_posts.html';
				//create component for file writing
				var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
				file.initWithPath( theFile );
				if(file.exists() == false) //check to see if file exists
				{
				    file.create( Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 766);
				}
				var foStream = Components.classes["@mozilla.org/network/file-output-stream;1"].createInstance(Components.interfaces.nsIFileOutputStream);
				// use 0x02 | 0x10 to open file for appending.
				foStream.init(file, 0x02 | 0x10, 0666, 0);
				//foStream.init(file, 0x02 | 0x08 | 0x20, 0666, 0); 

				// if you are sure there will never ever be any non-ascii text in data you can
				// also call foStream.write(data, data.length) directly
				var converter = Components.classes["@mozilla.org/intl/converter-output-stream;1"].createInstance(Components.interfaces.nsIConverterOutputStream);
				converter.init(foStream, "UTF-8", 0, 0);
				converter.writeString(msg);
				converter.close(); // this closes foStream
			}
			catch (e)
			{
				try
				{
					//create proper path for file
					var theFile = tofpostdeleter.savePath+'deleted_tof_posts-2.html';
					//create component for file writing
					var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
					file.initWithPath( theFile );
					if(file.exists() == false) //check to see if file exists
					{
					    file.create( Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 766);
					}
					var foStream = Components.classes["@mozilla.org/network/file-output-stream;1"].createInstance(Components.interfaces.nsIFileOutputStream);
					// use 0x02 | 0x10 to open file for appending.
					foStream.init(file, 0x02 | 0x10, 0666, 0);
					//foStream.init(file, 0x02 | 0x08 | 0x20, 0666, 0); 

					// if you are sure there will never ever be any non-ascii text in data you can
					// also call foStream.write(data, data.length) directly
					var converter = Components.classes["@mozilla.org/intl/converter-output-stream;1"].createInstance(Components.interfaces.nsIConverterOutputStream);
					converter.init(foStream, "UTF-8", 0, 0);
					converter.writeString(msg);
					converter.close(); // this closes foStream
				}catch(e){
					//
				}
			}
		},
			
		run : function () {
			//get initial parameters 
			// SAVE PATH / POSTS TO PROCESS / DELETE ORDER
			tofpostdeleter.savePath = prompt('Enter a path to save TOF Posts ("deleted_tof_posts.html" will be created there)','D:\\') || 'D:\\';
			tofpostdeleter.maxPostsToDelete = prompt('Enter number of TOF Posts to save&delete') || 0;
			var sure = prompt('Are you sure to delete those posts? Type "YES" to delete, otherwise they will only be saved to disk.') || '';
			tofpostdeleter.deleteForSure = (sure === "YES")? true:false;
			tofpostdeleter.deleteLinks = [];
			tofpostdeleter.currentPage = 0;
			tofpostdeleter.deleted = 0;
			tofpostdeleter.deleteFail = 0;
			tofpostdeleter.token = '';
			
			//Initiate loader elements/visuals
			var loader = content.document.createElement("div");
			loader.setAttribute("id", "loader");
			loader.innerHTML = "<div style='text-align:center;position:fixed;top:30%;left:35%;width:30%;font-family:Arial;font-size:18px;font-weight:bold;background-color:rgba(80,80,80,0.8);border-radius:10px;padding:20px;'><div id='loaderMessage'>Initializing TOF Post Deleter</div><div id='loaderPercent'>0%</div></div>";
			content.document.body.appendChild(loader);

			//stereotypical DELETE URL
			//https://bitcointalk.org/index.php?action=deletemsg;topic=484815.240;msg=5365640;sesc=6fef5db2bdcdfdeb2546dbb24fda1932

			function step2(postsPage) {
				tofpostdeleter.token = postsPage.match(/;sesc=(.+?)\"/)[1];
				try {
					//get post contents to save
					var postContents = postsPage.match(/<td width="100%" height="80" colspan="3" valign="top" class="windowbg2">([\s\S]+?)<\/div>\s+?<\/td>/g);
					var postsLen = postContents.length;

					var lastPostsToDel = 0;
					//limits posts to save and delete
					if(postsLen+tofpostdeleter.deleteLinks.length >= tofpostdeleter.maxPostsToDelete)
					{
						lastPostsToDel = parseInt(tofpostdeleter.maxPostsToDelete)-tofpostdeleter.deleteLinks.length;
					} else if (postsLen < 20) { //if less than 20, it is the last page
						lastPostsToDel = postsLen;
						tofpostdeleter.maxPostsToDelete = postsLen+tofpostdeleter.deleteLinks.length;
					}

					var contents = [];
					
					for(var i in postContents)
					{
						if(lastPostsToDel==0 || i<lastPostsToDel)
						{
							//append the contents to the file
							post = postContents[i].match(/<div class="post">([\s\S]*)<\/div>/);
							if(post && post[0])
							{
								var postContent = post[0];
								tofpostdeleter.writeToFile("<div style='left:10%;width:80%;border:2px inset #4a5;margin:10px;padding:10px;border-radius:5px;font-size:15px;font-family:arial;'>"+((tofpostdeleter.currentPage*20)+parseInt(i)+1)+postContent);
								tofpostdeleter.writeToFile("</div>");
							}
							// tofpostdeleter.writeToFile("<div style='left:10%;width:80%;border:2px inset #4a5;margin:10px;padding:10px;border-radius:5px;font-size:15px;font-family:arial;'>"+((tofpostdeleter.currentPage*20)+parseInt(i)+1)+postContents[i]);
							// tofpostdeleter.writeToFile("</div>");
						}
					}
					
					//PREPARE FOR DELETION - GET LINKS
					var posts = postsPage.match(/(index.php\?topic=.+?\.msg[^a-zA-Z]+?#)(?!.*Quote from:)/g);
					for(var i in posts)
					{
						//Limit delete links count, we do not want a runaway delete
						if(lastPostsToDel==0 || i<lastPostsToDel)
						{
							postInfo = posts[i].match(/topic=(.+?)\.msg(.+?)\#/);
							var url = "https://bitcointalk.org/index.php?action=deletemsg;topic="+postInfo[1]+";msg="+postInfo[2]+";sesc="+tofpostdeleter.token;
							//fill delete links array
							tofpostdeleter.deleteLinks.push(url);
						}
					}

					//DELETE OPERATIONS BEGIN HERE
					if(tofpostdeleter.deleteLinks.length >= tofpostdeleter.maxPostsToDelete)
					{	
						if(tofpostdeleter.deleteForSure)
						{
							var deletingProcedure = function() {
								//log delete request
								//tofpostdeleter.writeToFile("<p>Starting deleting procedure..."+(tofpostdeleter.deleted+1)+"</p>");
								//send delete request
								// setTimeout(function(){
								// 	tofpostdeleter.deleted++;
								// 	content.document.getElementById('loaderPercent').innerHTML = parseInt((parseInt(currentPost)+1)*100/postCount) +"%";
								// 	if(currentPost == postCount-1)
								// 	{
								// 		clearInterval(requestTimer);
								// 		content.document.getElementById('loaderMessage').innerHTML = "All posts should be saved and deleted by now! Happy no-TOF! :)";
								// 		setTimeout(function(){
								// 			content.document.getElementById('loader').remove();
								// 		},2000);
								// 		//log delete request
								// 		tofpostdeleter.writeToFile("<p>Ending deleting procedure..."+postCount+"</p>");
								// 	}
								// },1000);
								var deleteUrl = tofpostdeleter.deleteLinks[tofpostdeleter.deleted];
								tofpostdeleter.loadXMLDoc(deleteUrl,function(data){
									tofpostdeleter.deleted++;
									if(data.match(/An Error Has Occurred/))
									{
										var extraText = "";
										tofpostdeleter.deleteFail++;
										if(tofpostdeleter.deleteFail%20 == 0)
										{
											extraText = "Refreshing token..";
											tofpostdeleter.loadXMLDoc(tofpostdeleter.showPostsUrl+";start=0",function(page){
												tofpostdeleter.token = page.match(/;sesc=(.+?)\"/)[1];
												tofpostdeleter.writeToFile("<p>Token refreshed: "+tofpostdeleter.token+"</p>");
											});
										}
										tofpostdeleter.writeToFile("<p>Cannot delete post #"+tofpostdeleter.deleted+": Not allowed. "+ extraText +"</p>");
									}
									content.document.getElementById('loaderMessage').innerHTML = "Deleting post "+tofpostdeleter.deleted;
									content.document.getElementById('loaderPercent').innerHTML = parseInt((parseInt(currentPost)+1)*100/postCount) +"%";
									if(tofpostdeleter.deleted == tofpostdeleter.deleteLinks.length)
									{
										clearInterval(requestTimer);
										content.document.getElementById('loaderMessage').innerHTML = "All posts should be saved and deleted by now! Happy no-TOF! :)";
										setTimeout(function(){
											content.document.getElementById('loader').remove();
										},2000);
										//log delete request
										tofpostdeleter.writeToFile("<p>Ending deleting procedure...Failed to delete "+tofpostdeleter.deleteFail+" posts ... Successfully Deleted "+parseInt(tofpostdeleter.deleted-tofpostdeleter.deleteFail)+"posts</p>");
									}
								});
							}
							//Iterate requests
							var requestTimer = setInterval(
								function(){
									//set scope vars
									postCount = tofpostdeleter.deleteLinks.length;
									currentPost = tofpostdeleter.deleted;
									
									deletingProcedure();
								},2000);
						} else {
							content.document.getElementById('loaderPercent').innerHTML = "100%";
							content.document.getElementById('loaderMessage').innerHTML = "All posts should be saved now! :)";
							setTimeout(function(){
								content.document.getElementById('loader').remove();
							},2000);
						}
					} else {
						content.document.getElementById('loaderMessage').innerHTML = "Writing contents to file...";
						content.document.getElementById('loaderPercent').innerHTML = parseInt(tofpostdeleter.deleteLinks.length*100/tofpostdeleter.maxPostsToDelete) +"%\n";
						nextPagePostsUrl = tofpostdeleter.showPostsUrl+";start="+(tofpostdeleter.currentPage+1)*20;
						setTimeout(function(){
							if(tofpostdeleter.deleteLinks.length < tofpostdeleter.maxPostsToDelete)
							{
								tofpostdeleter.loadXMLDoc(nextPagePostsUrl,step2);
							}
						},500);	
					}
					//next page
					tofpostdeleter.currentPage++;
				} catch (e)	{
					alert(e+"\nDeleted "+tofpostdeleter.deleted+" posts\n");
				}
			}

			function step1(profilePage) {
				//Get posts url from profile page
				try{
					tofpostdeleter.showPostsUrl = profilePage.match(/https:\/\/bitcointalk.org\/index.php\?action=profile\;u=.+?\;sa=showPosts/);
				//var showPostsUrl = doc.getElementsByClassName('windowbg2')[1].getElementsByTagName('a')[2].href;
				} catch(e) {
					alert(e);
				}
				//Begin requests to get first posts page
				tofpostdeleter.loadXMLDoc(tofpostdeleter.showPostsUrl+";start=0",step2);
			}

			//Check login to TOF
			function checkLogin(loginPage) {
				var parser = new DOMParser();
				var doc = parser.parseFromString(loginPage, "text/xml");
				if(doc.getElementsByClassName('titlebg2').length == 4)
				{
					setTimeout(function(){
						content.document.getElementById('loaderMessage').innerHTML = "Detected TOF Login. Getting posts...";
						content.document.getElementById('loaderPercent').innerHTML = "1%";
					},200);
					tofpostdeleter.loadXMLDoc('https://bitcointalk.org/index.php?action=profile',step1);
				} else {
					alert('Please login to TOF first and run TOF Post Deleter again!');
					setTimeout(function(){
						content.document.getElementById('loader').remove();
					},1000);					
				}
			}

			if(tofpostdeleter.maxPostsToDelete > 0)
			{
				setTimeout(function(){
					content.document.getElementById('loaderMessage').innerHTML = "Checking TOF Login...";
				},200);
				var url = 'https://bitcointalk.org/index.php?action=login';
				tofpostdeleter.loadXMLDoc(url,checkLogin);
			} else {
				alert('No posts is saved. Operation aborted.');
				setTimeout(function(){
					content.document.getElementById('loader').remove();
				},1000);
			}
		}
	};
}();

window.addEventListener("load", tofpostdeleter.init, false);